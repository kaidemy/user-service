package business

import (
	"context"
	"fmt"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	constantsCommon "gitlab.com/kaidemy/common/constants"
	serviceCommon "gitlab.com/kaidemy/common/services"
	"gitlab.com/kaidemy/user-service/constants"
	"gitlab.com/kaidemy/user-service/models"
	"gitlab.com/kaidemy/user-service/service"
	"gitlab.com/kaidemy/user-service/utils"
)

type UserRepository interface {
	AddUser(c context.Context, user *models.User) error
	FindUserByEmail(c context.Context, email string) (*models.User, error)
	FindUserByID(c context.Context, userId int) (*models.User, error)
	FindUserByEmailToken(c context.Context, emailToken string) (*models.User, error)
	ChangePassword(c context.Context, newPassword string, userId int) error
	UpdateEmailToken(c context.Context, token *string, userId int) error
	UpdateProfile(c context.Context, updateProfile *models.UpdateProfile, userId int) error
}

type business struct {
	repository UserRepository
}

func NewBusiness(repository UserRepository) *business {
	return &business{
		repository,
	}
}

func (biz *business) Register(ctx context.Context, userCreate *models.UserCreate) (string, error) {
	if userCreate.TypeAccount == constants.AccountNormal {
		hashPassword, err := utils.HashPassword(userCreate.Password)
		if err != nil {
			return "", err
		}

		userCreate.Password = hashPassword
	}

	newUser := &models.User{
		Name:        userCreate.Name,
		Password:    userCreate.Password,
		TypeAccount: userCreate.TypeAccount,
		Role:        constantsCommon.NORMAL_USER,
		Email:       userCreate.Email,
	}

	if err := biz.repository.AddUser(ctx, newUser); err != nil {
		return "", err
	}

	token := serviceCommon.Encode(newUser.ID, constantsCommon.ROLE(newUser.Role))
	fmt.Println(newUser)

	return token, nil
}

func (biz *business) Login(ctx context.Context, userLogin *models.UserLogin) (*models.User, string, error) {
	user, err := biz.repository.FindUserByEmail(ctx, userLogin.Email)
	fmt.Println(err)

	if err != nil {
		return nil, "", err
	}

	if user == nil {
		return nil, "", constantsCommon.ErrUnAuthorize
	}

	if !utils.ComparePassword(userLogin.Password, user.Password) {
		return nil, "", constantsCommon.ErrUnAuthorize
	}

	if user.TypeAccount != constants.AccountNormal {
		return nil, "", constantsCommon.ErrUnAuthorize
	}

	token := serviceCommon.Encode(user.ID, constantsCommon.ROLE(user.Role))

	return user, token, nil
}

func (biz *business) ChangePassword(ctx context.Context, changePassword *models.ChangePassword, userId int) error {
	user, err := biz.repository.FindUserByID(ctx, userId)

	if err != nil {
		return err
	}

	if user == nil {
		return models.ErrUserNotFound
	}

	if !utils.ComparePassword(changePassword.OldPassword, user.Password) {
		return models.ErrPasswordNotExact
	}

	newPasswordHash, err := utils.HashPassword(changePassword.NewPassword)

	if err != nil {
		return err
	}

	return biz.repository.ChangePassword(ctx, newPasswordHash, userId)
}

func (biz *business) ForgotPassword(c context.Context, forgotPassword *models.ForgotPassword) error {
	user, err := biz.repository.FindUserByEmail(c, forgotPassword.Email)

	if err != nil {
		return err
	}

	if user.TypeAccount != constants.AccountNormal {
		return models.ErrOnlyForgotPasswordNormal
	}

	token := serviceCommon.EncodeEmail(forgotPassword.Email)

	if err := biz.repository.UpdateEmailToken(c, &token, user.ID); err != nil {
		return err
	}

	service.SendMailForgotPassword(forgotPassword.Email, token)

	return nil
}

func (biz *business) ResetPassword(c context.Context, resetPassword *models.ResetPassword) error {
	user, err := biz.repository.FindUserByEmailToken(c, resetPassword.EmailToken)

	if err != nil {
		return err
	}

	if user.TypeAccount != constants.AccountNormal {
		return models.ErrOnlyForgotPasswordNormal
	}
	claims := &jwt.StandardClaims{}

	token, err := jwt.ParseWithClaims(resetPassword.EmailToken, claims, func(token *jwt.Token) (interface{}, error) {
		return constantsCommon.JwtSecret, nil
	})

	if err != nil {
		return models.ErrEmailTokenInvalid
	}
	if !token.Valid {
		return models.ErrEmailTokenInvalid
	}

	newPasswordHash, err := utils.HashPassword(resetPassword.Password)

	if err != nil {
		return err
	}

	if err := biz.repository.ChangePassword(c, newPasswordHash, user.ID); err != nil {
		return err
	}

	if err := biz.repository.UpdateEmailToken(c, nil, user.ID); err != nil {
		return err
	}

	return nil
}

func (biz *business) UpdateProfile(c *fiber.Ctx, updateProfile *models.UpdateProfileForm, userId int) error {
	user, err := biz.repository.FindUserByID(c.Context(), userId)

	if err != nil {
		return err
	}

	if user == nil {
		return models.ErrUserNotFound
	}
	var uploadLink *string

	if updateProfile.Avatar != nil {
		uploadLinkGen := utils.GenerateUploadLink(updateProfile.Avatar.Filename)

		f, err := updateProfile.Avatar.Open()

		if err != nil {
			return err
		}

		defer f.Close()
		bucketName := "kaidemy"

		pathUpload, err := service.UploadAvatarToMinIo(utils.MinIOConn, bucketName, uploadLinkGen, f, updateProfile.Avatar.Header["Content-Type"][0])
		
		if err != nil {
			return err
		}
		uploadLink = &pathUpload
	} else {
		uploadLink = new(string)
	}
	
	dataUpdate := &models.UpdateProfile{
		Name:        updateProfile.Name,
		Headline:    updateProfile.Headline,
		Biography:   updateProfile.Biography,
		WebsiteURL:  updateProfile.WebsiteURL,
		TwitterURL:  updateProfile.TwitterURL,
		FacebookURL: updateProfile.FacebookURL,
		LinkedInURL: updateProfile.LinkedInURL,
		YoutubeURL:  updateProfile.YoutubeURL,
		Avatar:      uploadLink,
	}


	return biz.repository.UpdateProfile(c.Context(), dataUpdate, userId)

}
