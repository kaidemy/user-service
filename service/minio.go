package service

import (
	"context"
	"mime/multipart"

	"github.com/minio/minio-go/v7"
	// "gitlab.com/kaidemy/user-service/utils"
)

func UploadAvatarToMinIo(client *minio.Client, bucketName string, objectName string, file multipart.File, fileType string) (string, error) {
	// Create the bucket if it doesn't exist
	err := client.MakeBucket(context.Background(), bucketName, minio.MakeBucketOptions{})
	if err != nil {
		exists, errBucketExists := client.BucketExists(context.Background(), bucketName)
		if errBucketExists == nil && exists {
			// Bucket already exists
		} else {
			return "", err
		}
	}

	policy := `{
		"Version": "2012-10-17",
		"Statement": [
			{
			"Action": [
				"s3:GetObject"
			],
			"Effect": "Allow",
			"Principal": "*",
			"Resource": [
				"arn:aws:s3:::` + bucketName + `/*"
			]
			}
		]
		}`

	err = client.SetBucketPolicy(context.Background(), bucketName, policy)
	if err != nil {
		return "", err
	}

	// Upload the object
	info, err := client.PutObject(context.Background(), bucketName, objectName, file, -1, minio.PutObjectOptions{
		ContentType: fileType, // Update with the appropriate content type
		// UserMetadata:    metadata,
	})

	if err != nil {
		return "", err
	}

	return info.Location, nil
}
