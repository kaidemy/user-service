package service

import (
	"context"
	"encoding/json"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	rabbitmqCommom "gitlab.com/kaidemy/common/services/rabbitmq"
	"gitlab.com/kaidemy/user-service/utils"
)

func SendMailForgotPassword(email string, token string) error {
	conn := utils.RabbitMQConn
	ch, err := conn.Channel()
	if err != nil {
		return err
	}

	q, err := ch.QueueDeclare(
		rabbitmqCommom.SEND_MAIL_FORGOT_PASSWORD, // name
		false,                                    // durable
		false,                                    // delete when unused
		false,                                    // exclusive
		false,                                    // no-wait
		nil,                                      // arguments
	)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

	defer cancel()

	if err != nil {
		return err
	}

	body := rabbitmqCommom.MailForgotPassword{
		Email: email,
		Token: token,
	}

	bodyByte, err := json.Marshal(body)

	if err != nil {
		return err
	}
	err = ch.PublishWithContext(ctx,
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        bodyByte,
		})

	if err != nil {
		return err
	}
	defer conn.Close()
	return nil
}
