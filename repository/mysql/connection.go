package mysql

import (
	"fmt"

	"gitlab.com/kaidemy/user-service/models"
	"gitlab.com/kaidemy/user-service/utils"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func ConnectDB() (*gorm.DB, error) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", utils.EnvConfigs.DBUser, utils.EnvConfigs.DBPass, utils.EnvConfigs.DBHost, utils.EnvConfigs.DBPort, utils.EnvConfigs.DBName)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		return nil, fmt.Errorf("failed to connect to the Database: %s", err)
	}

	fmt.Println("🚀 Connected Successfully to the Database")
	tableMigrate := []interface{}{}

	if !db.Migrator().HasTable(&models.User{}) {
		tableMigrate = append(tableMigrate, &models.User{})
	}

	db.AutoMigrate(tableMigrate...)
	fmt.Println("🚀 Migrate Successfully")

	return db, nil
}
