package mysql

import (
	"context"
	"fmt"

	"gitlab.com/kaidemy/user-service/models"
	"gitlab.com/kaidemy/user-service/utils"
	"gorm.io/gorm"
)

type mysqlRepo struct {
	db *gorm.DB
}

func NewMySQLRepository(db *gorm.DB) *mysqlRepo {
	return &mysqlRepo{
		db: db,
	}
}

func (mysqlRepo *mysqlRepo) AddUser(c context.Context, user *models.User) error {
	if err := mysqlRepo.db.Create(user).Error; err != nil {
		if utils.CheckDuplicateEmail(err) {
			return models.ErrAlreadyExistEmail
		}
		return err
	}
	return nil
}

func (mysqlRepo *mysqlRepo) FindUserByEmail(c context.Context, email string) (*models.User, error) {
	user :=  &models.User{}
	
	if err := mysqlRepo.db.Table(user.TableName()).Where("email = ?", email).First(&user).Error; err != nil {
		fmt.Println(err)
		if err == gorm.ErrRecordNotFound {
			return nil, models.ErrUserNotFound
		}
		return nil, err
	}
	return user, nil
}

func (mysqlRepo *mysqlRepo) FindUserByID(c context.Context, id int) (*models.User, error) {
	user :=  &models.User{}
	
	if err := mysqlRepo.db.Table(user.TableName()).Where("id = ?", id).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, models.ErrUserNotFound
		}
		return nil, err
	}
	return user, nil
}

func (mysqlRepo *mysqlRepo) ChangePassword(c context.Context, newPassword string, userID int) error {
	user :=  &models.User{
		ID: userID,
	}

	return mysqlRepo.db.Model(&user).Update("password", newPassword).Error
}

func (mysqlRepo *mysqlRepo) UpdateEmailToken(c context.Context, token *string, userID int) error {
	user :=  &models.User{
		ID: userID,
	}
	return mysqlRepo.db.Model(&user).Update("email_token", token).Error
}

func (mysqlRepo *mysqlRepo) FindUserByEmailToken(c context.Context, emailToken string) (*models.User, error) {
	user :=  &models.User{}
	
	if err := mysqlRepo.db.Table(user.TableName()).Where("email_token = ?", emailToken).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, models.ErrEmailTokenInvalid
		}
		return nil, err
	}
	return user, nil
}

func (mysqlRepo *mysqlRepo) UpdateProfile(c context.Context, updateProfile *models.UpdateProfile, userID int) error {
	user :=  &models.User{
		ID: userID,
	}

	return mysqlRepo.db.Model(&user).Updates(updateProfile).Error
}