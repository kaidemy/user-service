package constants

var (
	AccountNormal   = 0
	AccountGoogle   = 1
	AccountFacebook = 2
)

var (
	UpdateSuccess         = "Update successfully"
	SendMailForgotSuccess = "Send mail forgot password successfully"
	ResetSuccess          = "Reset password successfully"
)
