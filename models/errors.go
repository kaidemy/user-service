package models

import "errors"

var (
	ErrPasswordIsNotValid       = errors.New("password must have from 8 to 30 characters")
	ErrNameTooLong              = errors.New("name too long, max character is 30")
	ErrNameTooShort             = errors.New("name too short, min character is 4")
	ErrNameAlreadyExist         = errors.New("name already exist")
	ErrUserNotFound             = errors.New("User not found")
	ErrPasswordNotExact         = errors.New("password not exact")
	ErrInvalidEmail             = errors.New("email invalid")
	ErrAlreadyExistEmail        = errors.New("email already existed")
	ErrOnlyForgotPasswordNormal = errors.New("forgot password only available for not social account")
	ErrEmailTokenInvalid        = errors.New("email token invalid")

	ErrHeadlineTooLong  = errors.New("headline too long, max character is 255")
	ErrHeadlineTooShort = errors.New("headline too short, min character is 1")

	ErrBiographyTooLong  = errors.New("biography too long, max character is 255")
	ErrBiographyTooShort = errors.New("biography too short, min character is 1")

	ErrWebsiteURLTooLong  = errors.New("website url too short, min character is 255")
	ErrWebsiteURLTooShort = errors.New("website url too short, min character is 1")

	ErrTwitterURLTooLong  = errors.New("twitter url too short, min character is 255")
	ErrTwitterURLTooShort = errors.New("twitter url too short, min character is 1")

	ErrFacebookURLTooLong  = errors.New("facebook url too short, min character is 255")
	ErrFacebookURLTooShort = errors.New("facebook url too short, min character is 1")

	ErrLinkedInURLTooLong  = errors.New("linked in url too short, min character is 255")
	ErrLinkedInURLTooShort = errors.New("linked in url too short, min character is 1")

	ErrYoutubeURLTooLong  = errors.New("youtube url too short, min character is 255")
	ErrYoutubeURLTooShort = errors.New("youtube url too short, min character is 1")

	ErrAvatarToMax      = errors.New("avatar too large, max size is 5MB")
	ErrAvatarNotSupport = errors.New("avatar not support, support image/png, image/jpg, image/jpeg")

	ErrCannotOpenFile = errors.New("cannot open file")
)
