package models

import (
	"mime/multipart"
	"regexp"
)

func checkPassword(s string) error {
	if len(s) < 8 || len(s) > 30 {
		return ErrPasswordIsNotValid
	}

	return nil
}

func checkNameValid(s string) error {
	if len(s) < 4 {
		return ErrNameTooShort
	}

	if len(s) > 30 {
		return ErrNameTooLong
	}

	return nil
}

func checkValidEmail(email string) error {
	// Regular expression to validate email format
	regex := `^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$`
	match, _ := regexp.MatchString(regex, email)

	if !match {
		return ErrInvalidEmail
	}

	return nil
}

func checkHeadline(headline string) error {
	if len(headline) < 1 {
		return ErrHeadlineTooShort
	}

	if len(headline) > 255 {
		return ErrHeadlineTooLong
	}

	return nil
}

func checkBiography(biography string) error {
	if len(biography) < 1 {
		return ErrHeadlineTooShort
	}

	if len(biography) > 255 {
		return ErrHeadlineTooLong
	}

	return nil
}

func checkWebsiteURL(websiteURL string) error {
	if len(websiteURL) < 1 {
		return ErrWebsiteURLTooShort
	}

	if len(websiteURL) > 255 {
		return ErrWebsiteURLTooLong
	}

	return nil
}

func checkTwitterURL(twitterURL string) error {
	if len(twitterURL) < 1 {
		return ErrTwitterURLTooShort
	}

	if len(twitterURL) > 255 {
		return ErrTwitterURLTooLong
	}

	return nil
}

func checkFacebookURL(facebookURL string) error {
	if len(facebookURL) < 1 {
		return ErrFacebookURLTooShort
	}

	if len(facebookURL) > 255 {
		return ErrFacebookURLTooLong
	}

	return nil
}

func checkLinkedInURL(linkedInURL string) error {
	if len(linkedInURL) < 1 {
		return ErrLinkedInURLTooShort
	}

	if len(linkedInURL) > 255 {
		return ErrLinkedInURLTooLong
	}

	return nil
}

func checkYoutubeURL(youtubeURL string) error {
	if len(youtubeURL) < 1 {
		return ErrYoutubeURLTooShort
	}

	if len(youtubeURL) > 255 {
		return ErrYoutubeURLTooLong
	}

	return nil
}

func checkAvatar(avatar *multipart.FileHeader) error {
	allowFileType := []string{"image/png", "image/jpg", "image/jpeg"}
	if avatar.Size > 1048576*5 {
		return ErrAvatarToMax
	}

	isSupportType := false
	for _, v := range allowFileType {
		if v == avatar.Header["Content-Type"][0] {
			isSupportType = true
		}
	}
	if !isSupportType {
		return ErrAvatarNotSupport

	}
	return nil
}
