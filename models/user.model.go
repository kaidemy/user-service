package models

import (
	"mime/multipart"
	"time"

	"gitlab.com/kaidemy/user-service/constants"
)

type User struct {
	ID          int        `json:"id" gorm:"primary_key"`
	Email       string     `json:"email" gorm:"type:varchar(255);not null;unique"`
	Name        string     `json:"name" gorm:"type:varchar(255);not null"`
	Password    string     `json:"-" gorm:"type:varchar(255);not null"`
	TypeAccount int        `json:"type_account" gorm:"not null"`
	Avatar      string     `json:"avatar" gorm:"type:varchar(255)"`
	Headline    string     `json:"headline" gorm:"type:varchar(255)"`
	Biography   string     `json:"biography" gorm:"type:varchar(255)"`
	WebsiteURL  string     `json:"website_url" gorm:"type:varchar(255)"`
	TwitterURL  string     `json:"twitter_url" gorm:"type:varchar(255)"`
	FacebookURL string     `json:"facebook_url" gorm:"type:varchar(255)"`
	LinkedInURL string     `json:"linkedin_url" gorm:"type:varchar(255)"`
	YoutubeURL  string     `json:"youtube_url" gorm:"type:varchar(255)"`
	Role        int        `json:"role" gorm:"not null"`
	EmailToken  *string    `json:"-" gorm:"type:varchar(255)"`
	UpdatedAt   *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt   *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (User) TableName() string { return "users" }

type UserCreate struct {
	Name        string `json:"name" form:"name"`
	Email       string `json:"email" form:"email"`
	Password    string `json:"-" form:"password"`
	TypeAccount int    `json:"type_account"`
}

func (uc *UserCreate) Validate() error {
	if uc.TypeAccount == constants.AccountNormal {
		if err := checkPassword(uc.Password); err != nil {
			return err
		}
	}

	if err := checkNameValid(uc.Name); err != nil {
		return err
	}

	if err := checkValidEmail(uc.Email); err != nil {
		return err
	}

	return nil
}

type UserLogin struct {
	Email    string `json:"email" form:"email"`
	Password string `json:"password" form:"password"`
}

func (uc *UserLogin) Validate() error {
	if err := checkPassword(uc.Password); err != nil {
		return err
	}

	if err := checkNameValid(uc.Password); err != nil {
		return err
	}

	return nil
}

type ChangePassword struct {
	OldPassword string `json:"old_password" form:"old_password"`
	NewPassword string `json:"new_password" form:"new_password"`
}

func (cp *ChangePassword) Validate() error {
	if err := checkPassword(cp.OldPassword); err != nil {
		return err
	}

	if err := checkPassword(cp.NewPassword); err != nil {
		return err
	}

	return nil
}

type ForgotPassword struct {
	Email string `json:"email" form:"email"`
}

func (fp *ForgotPassword) Validate() error {
	return checkValidEmail(fp.Email)
}

type ResetPassword struct {
	EmailToken string `json:"email_token" form:"email_token"`
	Password   string `json:"password" form:"password"`
}

func (rq *ResetPassword) Validate() error {
	return checkPassword(rq.Password)
}

type UpdateProfileForm struct {
	Name        *string               `json:"name" form:"name"`
	Headline    *string               `json:"headline" form:"headline"`
	Biography   *string               `json:"biography" form:"biography"`
	WebsiteURL  *string               `json:"website_url" form:"website_url"`
	TwitterURL  *string               `json:"twitter_url" form:"twitter_url"`
	FacebookURL *string               `json:"facebook_url" form:"facebook_url"`
	LinkedInURL *string               `json:"linkedin_url" form:"linkedin_url"`
	YoutubeURL  *string               `json:"youtube_url" form:"youtube_url"`
	Avatar      *multipart.FileHeader `json:"avatar" form:"avatar"`
}

type UpdateProfile struct {
	Name        *string `json:"name"`
	Headline    *string `json:"headline"`
	Biography   *string `json:"biography"`
	WebsiteURL  *string `json:"website_url"`
	TwitterURL  *string `json:"twitter_url"`
	FacebookURL *string `json:"facebook_url"`
	LinkedInURL *string `json:"linkedin_url"`
	YoutubeURL  *string `json:"youtube_url"`
	Avatar      *string `json:"avatar"`
}

func (up *UpdateProfileForm) Validate() error {
	if *up.Name != "" {
		if err := checkNameValid(*up.Name); err != nil {
			return err
		}
	}

	if *up.Headline != "" {
		if err := checkHeadline(*up.Headline); err != nil {
			return err
		}
	}

	if *up.Biography != "" {
		if err := checkBiography(*up.Biography); err != nil {
			return err
		}
	}

	if *up.WebsiteURL != "" {
		if err := checkWebsiteURL(*up.WebsiteURL); err != nil {
			return err
		}
	}

	if *up.TwitterURL != "" {
		if err := checkTwitterURL(*up.TwitterURL); err != nil {
			return err
		}
	}

	if *up.FacebookURL != "" {
		if err := checkFacebookURL(*up.FacebookURL); err != nil {
			return err
		}
	}

	if *up.LinkedInURL != "" {
		if err := checkLinkedInURL(*up.LinkedInURL); err != nil {
			return err
		}
	}

	if *up.YoutubeURL != "" {
		if err := checkYoutubeURL(*up.YoutubeURL); err != nil {
			return err
		}
	}

	if up.Avatar != nil {
		if err := checkAvatar(up.Avatar); err != nil {
			return err
		}
	}

	return nil
}
