package composer

import (
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
	mysqlRepository "gitlab.com/kaidemy/user-service/repository/mysql"
	business "gitlab.com/kaidemy/user-service/business"
	api "gitlab.com/kaidemy/user-service/transport/api"
)

type Service interface {
	RegisterHdl() func(*fiber.Ctx) error
	RequestLoginByGoogleHdl() func(*fiber.Ctx) error
	RequestLoginByFacebookHdl() func(*fiber.Ctx) error
	LoginByGoogleHdl() func(*fiber.Ctx) error
	LoginByFacebookHdl() func(*fiber.Ctx) error
	LoginNormalHdl() func(*fiber.Ctx) error
	ChangePasswordHdl() func(*fiber.Ctx) error
	ForgotPasswordHdl() func(*fiber.Ctx) error
	ResetPasswordHdl() func(*fiber.Ctx) error
	UpdateProfileHdl() func(*fiber.Ctx) error
}

func ComposeAPIService(db *gorm.DB) Service {
	repo := mysqlRepository.NewMySQLRepository(db)
	biz := business.NewBusiness(repo)
	service := api.NewAPI(biz)

	return service
}