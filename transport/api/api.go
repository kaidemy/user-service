package api

import (
	"context"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/kaidemy/user-service/models"
)

type Business interface {
	Register(c context.Context, userCreate *models.UserCreate) (string, error)
	Login(c context.Context, userLogin *models.UserLogin) (*models.User, string, error)
	ChangePassword(c context.Context, changePassword *models.ChangePassword, userId int) error
	ForgotPassword(c context.Context, forgotPassword *models.ForgotPassword) error
	ResetPassword(c context.Context, resetPassword *models.ResetPassword) error
	UpdateProfile(c *fiber.Ctx, updateProfile *models.UpdateProfileForm, userId int) error
}

type api struct {
	business Business
}

func NewAPI(business Business) *api {
	return &api{
		business,
	}
}