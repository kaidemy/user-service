package api

import (
	"net/http"

	"github.com/gofiber/fiber/v2"
	constantsCommon "gitlab.com/kaidemy/common/constants"
	utilsCommon "gitlab.com/kaidemy/common/utils"
	"gitlab.com/kaidemy/user-service/constants"
	"gitlab.com/kaidemy/user-service/models"
)

func (a *api) ChangePasswordHdl() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		var changePassword models.ChangePassword

		if err := c.BodyParser(&changePassword); err != nil {
			if err.Error() != "Unprocessable Entity" {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": constantsCommon.ErrServer.Error(),
				})
			}
		}

		if err := changePassword.Validate(); err != nil {
			return c.Status(http.StatusUnprocessableEntity).JSON(fiber.Map{
				"message": err.Error(),
			})
		}

		userId := utilsCommon.GetUserId(c)

		if err := a.business.ChangePassword(c.Context(), &changePassword, userId); err != nil {
			if err == models.ErrPasswordNotExact {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": models.ErrPasswordNotExact.Error(),
				})
			}

			return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
				"message": constantsCommon.ErrServer.Error(),
			})
		}

		return c.Status(http.StatusOK).JSON(fiber.Map{
			"message": constants.UpdateSuccess,
		})
	}
}

func (a *api) UpdateProfileHdl() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		var updateProfile models.UpdateProfileForm
		if err := c.BodyParser(&updateProfile); err != nil {
			if err.Error() != "Unprocessable Entity" {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": constantsCommon.ErrServer.Error(),
				})
			}
		}
		file, err := c.FormFile("avatar")
		
		if err != nil {
			if err.Error() != "there is no uploaded file associated with the given key" {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": constantsCommon.ErrServer.Error(),
				})
			}
		}
		updateProfile.Avatar = file

		if err := updateProfile.Validate(); err != nil {
			return c.Status(http.StatusUnprocessableEntity).JSON(fiber.Map{
				"message": err.Error(),
			})
		}

		userId := utilsCommon.GetUserId(c)
		if err := a.business.UpdateProfile(c, &updateProfile, userId); err != nil {
			if err == models.ErrUserNotFound {
				return c.Status(http.StatusUnauthorized).JSON(fiber.Map{
					"message": models.ErrUserNotFound.Error(),
				})
			}

			return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
				"message": constantsCommon.ErrServer.Error(),
			})
		}

		return c.Status(http.StatusOK).JSON(fiber.Map{
			"message": constants.UpdateSuccess,
		})
	}
}
