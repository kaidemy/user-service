package api

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/gofiber/fiber/v2"
	constantsCommon "gitlab.com/kaidemy/common/constants"
	utilsCommon "gitlab.com/kaidemy/common/utils"
	"gitlab.com/kaidemy/user-service/models"
	"golang.org/x/oauth2"

	"gitlab.com/kaidemy/user-service/constants"
	oauthConfig "gitlab.com/kaidemy/user-service/utils/oauth"
)

func (a *api) RequestLoginByGoogleHdl() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {

		url := oauthConfig.OauthConfigGoogle.AuthCodeURL("", oauth2.AccessTypeOffline)
		return c.Redirect(url)
	}
}

func (a *api) RequestLoginByFacebookHdl() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		url := oauthConfig.OauthConfigFacebook.AuthCodeURL("state")
		fmt.Println(url)
		return c.Redirect(url)
	}
}

func (a *api) LoginByGoogleHdl() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {

		code := c.Query("code")
		token, err := oauthConfig.OauthConfigGoogle.Exchange(c.Context(), code)
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).SendString("Error exchanging token")
		}
		// Use the token to fetch user info or perform other actions
		// token.AccessToken contains the access token
		client := oauthConfig.OauthConfigGoogle.Client(c.Context(), token)
		response, err := client.Get("https://www.googleapis.com/oauth2/v2/userinfo")
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).SendString("Error fetching user info")
		}

		defer response.Body.Close()

		// Read the response body
		body, err := io.ReadAll(response.Body)
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).SendString("Error reading response")
		}

		var userCreate *models.UserCreate

		json.Unmarshal(body, &userCreate)
		userCreate.TypeAccount = constants.AccountGoogle

		if err := userCreate.Validate(); err != nil {
			return c.Status(http.StatusUnprocessableEntity).JSON(fiber.Map{
				"message": err.Error(),
			})
		}
		tokenUser, err := a.business.Register(c.Context(), userCreate)
		// tokenUser
		fmt.Println(tokenUser)
		if err != nil {
			return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
				"message": constantsCommon.ErrServer.Error(),
			})
		}
		return c.Status(http.StatusCreated).JSON(utilsCommon.SimpleSuccessResponse(fiber.Map{
			"token": tokenUser,
			"user":  userCreate,
		}))
	}
}

func (a *api) LoginByFacebookHdl() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {

		code := c.Query("code")
		token, err := oauthConfig.OauthConfigFacebook.Exchange(c.Context(), code)
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).SendString("Error exchanging token")
		}
		fmt.Println(token)

		return nil
	}
}

func (a *api) LoginNormalHdl() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		var userLogin models.UserLogin

		if err := c.BodyParser(&userLogin); err != nil {
			if err.Error() != "Unprocessable Entity" {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": constantsCommon.ErrServer.Error(),
				})
			}
		}

		if err := userLogin.Validate(); err != nil {
			return c.Status(http.StatusUnprocessableEntity).JSON(fiber.Map{
				"message": err.Error(),
			})
		}

		user, token, err := a.business.Login(c.Context(), &userLogin)

		if err != nil {
			if err == constantsCommon.ErrUnAuthorize {
				return c.Status(http.StatusUnauthorized).JSON(fiber.Map{
					"message": constantsCommon.ErrUnAuthorize.Error(),
				})
			}
			if err == models.ErrUserNotFound {
				return c.Status(http.StatusUnauthorized).JSON(fiber.Map{
					"message": models.ErrUserNotFound.Error(),
				})
			}

			return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
				"message": constantsCommon.ErrServer.Error(),
			})
		}
		return c.Status(http.StatusOK).JSON(utilsCommon.SimpleSuccessResponse(fiber.Map{
			"token": token,
			"user":  user,
		}))
	}
}

func (a *api) RegisterHdl() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		var userCreate models.UserCreate

		if err := c.BodyParser(&userCreate); err != nil {
			if err.Error() != "Unprocessable Entity" {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": constantsCommon.ErrServer.Error(),
				})
			}
		}

		if err := userCreate.Validate(); err != nil {
			return c.Status(http.StatusUnprocessableEntity).JSON(fiber.Map{
				"message": err.Error(),
			})
		}
		userCreate.TypeAccount = constants.AccountNormal

		tokenUser, err := a.business.Register(c.Context(), &userCreate)

		if err != nil {
			if err == models.ErrAlreadyExistEmail {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": models.ErrAlreadyExistEmail.Error(),
				})
			}
			return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
				"message": constantsCommon.ErrServer.Error(),
			})

		}
		return c.Status(http.StatusCreated).JSON(utilsCommon.SimpleSuccessResponse(fiber.Map{
			"token": tokenUser,
			"user":  userCreate,
		}))
	}
}

func (a *api) ForgotPasswordHdl() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		var forgotPassword models.ForgotPassword

		if err := c.BodyParser(&forgotPassword); err != nil {
			if err.Error() != "Unprocessable Entity" {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": constantsCommon.ErrServer.Error(),
				})
			}
		}
		if err := forgotPassword.Validate(); err != nil {
			return c.Status(http.StatusUnprocessableEntity).JSON(fiber.Map{
				"message": err.Error(),
			})
		}

		if err := a.business.ForgotPassword(c.Context(), &forgotPassword); err != nil {
			if err == models.ErrUserNotFound {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": models.ErrUserNotFound.Error(),
				})
			}

			if err == models.ErrOnlyForgotPasswordNormal {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": models.ErrOnlyForgotPasswordNormal.Error(),
				})
			}
			
			return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
				"message": constantsCommon.ErrServer.Error(),
			})
		}

		return c.Status(http.StatusCreated).JSON(fiber.Map{
			"message": constants.SendMailForgotSuccess,
		})
	}
}

func (a *api) ResetPasswordHdl() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		var resetPassword models.ResetPassword

		if err := c.BodyParser(&resetPassword); err != nil {
			if err.Error() != "Unprocessable Entity" {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": constantsCommon.ErrServer.Error(),
				})
			}
		}

		if err := resetPassword.Validate(); err != nil {
			return c.Status(http.StatusUnprocessableEntity).JSON(fiber.Map{
				"message": err.Error(),
			})
		}

		if err := a.business.ResetPassword(c.Context(), &resetPassword); err != nil {
			if err == models.ErrUserNotFound {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": models.ErrUserNotFound.Error(),
				})
			}
			if err == models.ErrOnlyForgotPasswordNormal {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": models.ErrOnlyForgotPasswordNormal.Error(),
				})
			}
			
			if err == models.ErrEmailTokenInvalid {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"message": models.ErrEmailTokenInvalid.Error(),
				})
			}

			return c.Status(http.StatusInternalServerError).JSON(fiber.Map{

				"message": constantsCommon.ErrServer.Error(),
			})
		}

		return c.Status(http.StatusOK).JSON(fiber.Map{
			"message": constants.ResetSuccess,
		})
	}
}
