package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/kaidemy/user-service/composer"
	"gorm.io/gorm"
)

func Init(app *fiber.App, db *gorm.DB) {
	router := app.Group("/api/v1")
	apiService := composer.ComposeAPIService(db)

	AuthRouter(&router, &apiService)
	UserRouter(&router, &apiService)
}
