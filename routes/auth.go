package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/kaidemy/user-service/composer"
)

func AuthRouter(router *fiber.Router, service *composer.Service) {
	prefixRouter := (*router).Group("/auth")

	prefixRouter.Post("/register", (*service).RegisterHdl())
	prefixRouter.Post("/login", (*service).LoginNormalHdl())
	prefixRouter.Get("/login-google", (*service).RequestLoginByGoogleHdl())
	prefixRouter.Get("/google/callback", (*service).LoginByGoogleHdl())
	prefixRouter.Get("/login-facebook", (*service).RequestLoginByFacebookHdl())
	prefixRouter.Get("/facebook/callback", (*service).LoginByFacebookHdl())
	
	prefixRouter.Post("/forgot-password", (*service).ForgotPasswordHdl())
	prefixRouter.Post("/reset-password", (*service).ResetPasswordHdl())
}
