package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/kaidemy/user-service/composer"
	"gitlab.com/kaidemy/common/middleware"
)

func UserRouter(router *fiber.Router, service *composer.Service) {
	prefixRouter := (*router).Group("/users", middleware.AuthMiddleware)
	
	prefixRouter.Post("/change-password", (*service).ChangePasswordHdl())
	prefixRouter.Post("/profile", (*service).UpdateProfileHdl())
	// prefixRouter.Post("/register", (*service).RegisterHdl())
	// prefixRouter.Post("/login", (*service).LoginNormalHdl())
	// prefixRouter.Get("/login-google", (*service).RequestLoginByGoogleHdl())
	// prefixRouter.Get("/login-facebook", (*service).RequestLoginByFacebookHdl())
	// prefixRouter.Get("/facebook/callback", (*service).LoginByFacebookHdl())

}
