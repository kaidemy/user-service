package oauth

import (
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/facebook"
	"golang.org/x/oauth2/google"
)

var (
	OauthConfigFacebook = oauth2.Config{
		ClientID:     "939678027101570",
		ClientSecret: "3dd6af04a8f2b80fa9a9ad6895d15983",
		RedirectURL:  "http://localhost:3000/api/v1/auth/facebook/callback",
		Scopes:       []string{"email"},
		Endpoint:     facebook.Endpoint,
	}
)

var (
	OauthConfigGoogle = oauth2.Config{
		ClientID:     "148930819635-mstvqp7ok9qfm6jg4pub82d8b4r4imk6.apps.googleusercontent.com",
		ClientSecret: "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",
		RedirectURL:  "http://localhost:3000/api/v1/auth/google/callback",
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"},
		Endpoint:     google.Endpoint,
	}
)