package utils

import (
	"math/rand"
	"fmt"
	"strconv"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(hashedPassword), nil
}

func ComparePassword(password string, passwordHash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(passwordHash), []byte(password))

	if err != nil {
		return false
	} else {
		return true
	}
}

func CheckDuplicateEmail(err error) bool {
	return strings.Contains( err.Error(), "Duplicate")
}

func GenerateUploadLink(nameFile string) string {
	uploadLink := fmt.Sprint(time.Now().UnixMilli()) + strconv.Itoa(rand.Int()) + nameFile
	return uploadLink
}
