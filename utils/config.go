package utils

import (
	"os"
	"path/filepath"

	"github.com/minio/minio-go/v7"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/spf13/viper"
)

type Configurations struct {
	// ServerAddress   string
	// ServerPort      string
	DBHost          string
	DBName          string
	DBUser          string
	DBPass          string
	DBPort          string
}

var EnvConfigs *Configurations
var RabbitMQConn *amqp.Connection
var MinIOConn *minio.Client

func InitConfigurations() {
	cwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	// Find the root directory of the project by looking for the go.mod file
	root := cwd
	for {
		if _, err := os.Stat(filepath.Join(root, "go.mod")); err == nil {
			break
		}
		root = filepath.Dir(root)

		if root == "/" || root == "." {
			panic("could not find root directory")
		}
	}
	dotEnvPath := filepath.Join(root, ".env")

	viper.AutomaticEnv()

	viper.SetConfigFile(dotEnvPath)
	viper.ReadInConfig()

	configs := &Configurations{
		DBHost: viper.GetString("DB_HOST"),
		DBName: viper.GetString("DB_NAME"),
		DBUser: viper.GetString("DB_USER"),
		DBPass: viper.GetString("DB_PASS"),
		DBPort: viper.GetString("DB_PORT"),
	}
	EnvConfigs = configs
}
