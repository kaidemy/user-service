package main

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"

	"gitlab.com/kaidemy/common/services/rabbitmq"
	"gitlab.com/kaidemy/common/services/minio"
	"gitlab.com/kaidemy/user-service/repository/mysql"
	"gitlab.com/kaidemy/user-service/routes"
	"gitlab.com/kaidemy/user-service/utils"
)

func main() {
	utils.InitConfigurations()

	db, err := mysql.ConnectDB()

	if err != nil {
		log.Fatal("Database Connection Error $s", err)
	}

	connRabbitMq, err := rabbitmq.ConnectRabbitMQ()

	if err != nil {
		log.Fatal("Cannot connect RabbitMQ", err)
	}

	connMinIO, err := minio.ConnectMinIO()

	if err != nil {
		log.Fatal("Cannot connect MinIO", err)
	}

	utils.RabbitMQConn = connRabbitMq
	utils.MinIOConn = connMinIO

	app := fiber.New()
	app.Static("/", "./upload")
	defer connRabbitMq.Close()

	routes.Init(app, db)
	zap.Error(app.Listen(":3000"))
}
